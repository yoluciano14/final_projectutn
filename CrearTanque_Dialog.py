from PyQt5 import QtWidgets
from ui_CrearTanque_Dialog import Ui_Dialog
import sys

class Dialog(Ui_Dialog):
    def __init__(self, parent=None):
        super().__init__()
        #super(Dialog, self).__init__(parent=parent)
        self.setupUi(self)
    
if __name__ == "__main__":
     app = QtWidgets.QApplication(sys.argv)
     dlge = QtWidgets.QDialog()
     ui = Ui_Dialog()
     ui.setupUi(dlge)

     dlge.show()
     sys.exit(app.exec_())