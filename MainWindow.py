from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QMessageBox
from ui_MainWindow import Ui_MainWindow
import sys

import webbrowser
import pathlib

from CrearTanque_Dialog import Dialog

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        ##########################################################################
        #Defino la funcion MainWindow para luego iniciar la interfaz como dios manda
        super(MainWindow, self).__init__(parent=parent)
        self.setupUi(self)
        ##########################################################################

        ##########################################################################
        #Defino las conexiones entre los botones de la interfaz
        self.stackedWidget.setCurrentWidget(self.page_home)
        self.btn_set_db.clicked.connect(self.abrir_DB)
        self.btn_ver_tanques.clicked.connect(self.abrir_Tanques)
        self.btn_home.clicked.connect(self.abrir_home)
        
        self.btn_new_test.clicked.connect(self.abrir_new) ##En desarollo...

        # self.btn_close.clicked.connect(self.cerrar_exe)##este no es tan importante por el momento (05/02)
        # self.btn_maximize_restore.clicked.connect(self.maximizar_exe) ##este no es tan importante por el momento (05/02)
        # self.btn_minimize.clicked.connect(self.minimizar_exe)  ##este no es tan importante por el momento (05/02)
        
        self.utn_button_3.clicked.connect(lambda: webbrowser.open('https://www.frc.utn.edu.ar'))
        self.utn_button_4.clicked.connect(lambda: webbrowser.open('https://www.fermentumfaber.com')) 


        
        ##########################################################################

    def abrir_DB(self):
        self.stackedWidget.setCurrentWidget(self.page_database)
    def abrir_Tanques(self):
        self.stackedWidget.setCurrentWidget(self.page_crear_tanque)
    def abrir_home(self):
        self.stackedWidget.setCurrentWidget(self.page_home)
    
    def abrir_new(self): #------- En desarrollo pues tiene que abrir el dialog "CrearTanque_Dialog.py"

        dlg= QtWidgets.QDialog()
        ui = Dialog()
        ui.setupUi(dlg)
        dlg.show()
        dlg.exec_()


##########################################################################
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
#Hasta acá es todo lo que si o si necesito para que se inicie o muestre la
#interfaz. La funcion .show() hace eso y
##########################################################################


# class MainWindow(QWidget, Ui_MainWindow):
#     def __init__(self):
#         super().__init__()
#         self.setupUi(self)
#         self.setWindowTitle("QFile Demo")

#         #Make the connections
#         self.write_button.clicked.connect(self.write_button_clicked)
#         self.read_button.clicked.connect(self.read_button_clicked)
#         self.select_file_button.clicked.connect(self.select_file_button_clicked)
#         self.copy_file_button.clicked.connect(self.copy_file_button_clicked)


#     def write_button_clicked(self):
#         file_name,_ = QFileDialog.getSaveFileName(self, "Save File",
#                                  "/home/jana/untitled.png",
#                                  "Text(*.txt);;Images (*.png *.xpm *.jpg);;All files(*.*)")
#         if((file_name == "")):
#             return
#         print("file :",file_name)
#         file = QFile(file_name)
#         if (not file.open(QIODevice.WriteOnly | QIODevice.Text)):
#             return
#         out = QTextStream(file)
#         out << self.textEdit.toPlainText() << "\n"
#         file.close()


#     def read_button_clicked(self):
#         file_content = ""
#         file_name,_ = QFileDialog.getOpenFileName(self, "Open File",
#                                  "/home/jana/untitled.png",
#                                  "Text(*.txt);;Images (*.png *.xpm *.jpg);;All files(*.*)")
#         if((file_name == "")):
#             return
#         print("Opened file :",file_name)
#         file = QFile(file_name)
#         if (not file.open(QIODevice.ReadOnly | QIODevice.Text)):
#             return
#         in_stream = QTextStream(file)
#         while (not in_stream.atEnd()) :
#             line = in_stream.readLine()
#             file_content += '\n'
#             file_content += line
#         file.close()
#         self.textEdit.clear()
#         self.textEdit.setText(file_content)


#     def select_file_button_clicked(self):
#         file_name,_ = QFileDialog.getOpenFileName(self, "Open File",
#                                  "/home/jana/untitled.png",
#                                  "Text(*.txt);;Images (*.png *.xpm *.jpg);;All files(*.*)")
#         if((file_name == "")):
#             return
#         self.source_line_edit.setText(file_name)

#     def copy_file_button_clicked(self):
#         src = self.source_line_edit.text()
#         dst = self.dest_line_edit.text()
#         if((src == "")or(dst == "")):
#             return

#         file = QFile(src)
#         if (file.copy(dst)):
#             QMessageBox.information(self,"Success","Copy successful")
#         else:
#             QMessageBox.information(self,"Failure","Copy Failed")